<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AuthModel extends CI_Model{

	/**
	* USE : Save user data
	**/
	public function userdata_store($data){
		$sql = $this->db->insert('users',$data);
		if($sql){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	public function checkCredential($email,$password){
		$this->db->select('*');		
		$this->db->where('email',$email);
		$this->db->where('password',md5($password));
		$sql = $this->db->get('users');
		if($sql){
			return $sql->row_array();
		}else{
			return false;
		}		
	}
}
