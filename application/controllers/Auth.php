<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('AuthModel');
	}

	/**
	 * USE : Register
	 */
	public function register(){
		
		if ($this->input->server('REQUEST_METHOD') == 'GET'){
			$data['title'] = 'Register';
			$this->load->view('register', $data);
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			
			$postData = array(
				'email' => trim($this->input->post('email')),
				'password' => md5($this->input->post('password'))
			);

			if(!empty($postData)){
				$insert_id = $this->AuthModel->userdata_store($postData);
				if($insert_id){
					$this->session->set_flashdata('success_msg', 'Registerd successfully.');
					redirect('login');
				}else{
					$this->session->set_flashdata('error_msg', 'Sorry problem was occured. Please try again.');
					redirect('register');
				}
			}

		}		
	}

	/**
	 * USE : Login
	 */
	public function login(){
		if($this->session->userdata('userid')){
			redirect('quiz');
		}
		if ($this->input->server('REQUEST_METHOD') == 'GET'){
			$data['title'] = 'Login';
			$this->load->view('login', $data);
		}
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$email = trim($this->input->post('email'));
			$password = $this->input->post('password');			

			$UserData = $this->AuthModel->checkCredential($email,$password);			
			if(!empty($UserData)){
				$this->session->set_userdata('userid',$UserData['id']);
				$this->session->set_flashdata('success_msg', 'Registerd successfully.');
				redirect('quiz');
			}else{
				$this->session->set_flashdata('error_msg', 'Invalid Login Credential... Please try again');
				redirect('login');
			}
		}		
	}

	// log the user out
	public function logout()
	{
		$this->data['title'] = "Logout";
		// log the user out
		$this->session->unset_userdata('userid');
		$logout = $this->session->sess_destroy();
		redirect('login');
	}

}
