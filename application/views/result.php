<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Your Result</h2>
  <?php
    if($correct_answer > 6){
				$result = 'Pass';
				$moment = 'Congretulations';
    }else{
				$result = 'Fail';
				$moment = 'Sorry';
    }
  ?>
  <b><h3><?php echo $result; ?></h3></b>
  <p><?php echo $moment; ?> You have <?php echo $result; ?> this quiz...</p>            
  <table class="table">
    <thead>
      <tr>
        <th>Total Quiestion</th>
        <th>Total Correct Answer</th>
        <th>Total Wrong Answer</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><?php echo 10; ?></td>
        <td><?php echo $correct_answer; ?></td>
        <td><?php echo $wrong_answer; ?></td>
      </tr>
    </tbody>
  </table>

  <a href="<?php echo base_url('remove_quiz'); ?>"><button>Play Again Quiz</button>
</div>

</body>
</html>
