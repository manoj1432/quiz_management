<html>
	<head>
	<link rel="stylesheet" href="http://localhost/Quiz_system/scripts/bootstrap/bootstrap.min.css">
	</head>
	<title>Quiz</title>
	<style>
		.error{
			color:red;
		}
		body{
			background-color: #eee;
		}
		.quiz-section .answer span{
			padding-right: 15px;
		}
		.quiz-section .answer .answer-ans{
			padding-bottom: 5px;
		}
		.quiz-section .answer .answer-ans span{
			padding-left: 6px;
		}
		.quiz-section .answer{
			padding-left: 47PX;
		}
		.quiz-section .question h4{
			font-size: 18px;
			line-height: 28px;
			font-weight: 700;
		}
		.quiz-section .question h4 span{
			font-size: 16px;
			line-height: 26px;
			font-weight: 600;
			padding-left: 10px;
		}
	</style>
	<body>
	<form id="quiz" action="<?php echo base_url('quizsubmit'); ?>" method="post">
		<div class="quiz-section">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<?php
						if(!empty($quizQuestion)){
							$QuesKey = 1;
							foreach($quizQuestion as $quizQuestion){ ?>
								<div class="quiz-section-inner">
									<div class="question">
										<h4>Q:<?php echo $QuesKey; ?>) <span><?php echo $quizQuestion['question']; ?></span><h4>
									</div>
									<span class="question_error_<?php echo $QuesKey;?>"></span>
									<?php if(!empty($quizQuestion['options'])){
										foreach($quizQuestion['options'] as $OptKey => $option){ ?>
											<div class="answer">
												<div class="answer-ans"><input type="radio" class="question_cls" name="quistion_<?php echo $QuesKey; ?>" value="<?php echo $option['id']; ?>"><span><?php echo $option['answer']; ?></span></div>
											</div>
									<?php }
									} ?>
								</div>
					<?php $QuesKey++; 
						}
						} ?>
						<button class="btn btn-primary" name="submit">Submit</button>
					</div>	
					
				</div>
				
			</div>
		</div>
		</form>
	</body>
	<script src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>scripts/bootstrap/bootstrap.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js"></script>

	<script>
	$(document).ready(function() {
		$('#quiz').validate({
			rules: {
				quistion_1:{
					required:true,
				},
				quistion_2:{
					required:true,
				},
				quistion_3:{
					required:true,
				},
				quistion_4:{
					required:true,
				},
				quistion_5:{
					required:true,
				},
				quistion_6:{
					required:true,
				},
				quistion_7:{
					required:true,
				},
				quistion_8:{
					required:true,
				},
				quistion_9:{
					required:true,
				},
				quistion_10:{
					required:true,
				},
			},
			messages: {
				
			},
			errorPlacement: function (error, element) {
				if(element.attr("name") == "quistion_1") {
					error.appendTo('.question_error_1');
				}else if(element.attr("name") == "quistion_2") {
					error.appendTo('.question_error_2');
				}else if(element.attr("name") == "quistion_3") {
					error.appendTo('.question_error_3');
				}else if(element.attr("name") == "quistion_4") {
					error.appendTo('.question_error_4');
				}else if(element.attr("name") == "quistion_5") {
					error.appendTo('.question_error_5');
				}else if(element.attr("name") == "quistion_6") {
					error.appendTo('.question_error_6');
				}else if(element.attr("name") == "quistion_7") {
					error.appendTo('.question_error_7');
				}else if(element.attr("name") == "quistion_8") {
					error.appendTo('.question_error_8');
				}else if(element.attr("name") == "quistion_9") {
					error.appendTo('.question_error_9');
				}else if(element.attr("name") == "quistion_10") {
					error.appendTo('.question_error_10');
				}
			},
			// errorElement: 'span',
			// errorPlacement: function(error, element) {
			// 	//error.insertBefore(element);
			// },
			submitHandler: function (form) {
				$("#cover-spin").css("display", "block");
				form.submit();
			}
		});
	});
	</script>
</html>
