<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class QuizController extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('QuizModel');
	}

	/**
	 * USE : Quiz Question List
	 */
	public function quiz(){

		if(!$this->session->userdata('userid')){
			redirect('login');
		}

		$data['title'] = 'Quiz';

		$questions = $this->QuizModel->getQuizQuestion();
		if(!empty($questions)){
			foreach($questions as $key => $question){
				$questions[$key]['options'] = $this->QuizModel->getAnswerByQuestion($question['id']);
			}
		}
		$data['quizQuestion'] = $questions;
		$this->load->view('quiz', $data);
	}

	/**
	 * USE : User can submit quiz detail
	 */
	public function quizSubmit(){
		if(!$this->session->userdata('userid')){
			redirect('login');
		}
		if($_POST){
			$userId = $this->session->userdata('userid');
			for($Question = 1; $Question <= 10; $Question++){
				$data = array(
					'user_id' => $userId,
					'quiestion_id' => $Question,
					'answer_id' => $this->input->post('quistion_'.$Question),
					'correct_answer' => $this->CheckAnswerCorrect($Question,$this->input->post('quistion_'.$Question)),
				);
				$insert_id = $this->QuizModel->saveQuiz($data);
			}
			redirect('result');
		}
		redirect('quiz');
	}

	public function CheckAnswerCorrect($Question,$selectedAnswer){
		if(!$this->session->userdata('userid')){
			redirect('login');
		}
		$CorrectAnswer = $this->QuizModel->getCorrectAnswerByQuestion($Question);
		if($CorrectAnswer === $selectedAnswer){
			return 1;
		}else{
			return 0;
		}
	}

	/**
	 * USE : Get User Quiz Result
	 */
	public function result(){
		if(!$this->session->userdata('userid')){
			redirect('login');
		}
		$userId = $this->session->userdata('userid');

		$data = [];
		$data['correct_answer'] = $this->QuizModel->countCorrectAnswer();
		$data['wrong_answer'] = (10 - $data['correct_answer']);

		$this->load->view('result', $data);
	}

	/**
	 * USE : Delete Quiz
	 */
	public function remove_quiz(){
		if(!$this->session->userdata('userid')){
			redirect('login');
		}
		$data = $this->QuizModel->deleteQuiz();
		if($data){
			redirect('quiz');
		}else{
			redirect('result');
		}
	}
}
