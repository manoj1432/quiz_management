<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class QuizModel extends CI_Model{

	/**
	 * Get Quision List
	 */
	public function getQuizQuestion(){
		$this->db->select('*');		
		$sql = $this->db->get('question');
		if($sql){
			return $sql->result_array();
		}else{
			return false;
		}		
	}

	/**
	 * Get Quision List
	 */
	public function getAnswerByQuestion($questionId){
		$this->db->select('*');
		$this->db->where('question_id',$questionId);	
		$sql = $this->db->get('answers');
		if($sql){
			return $sql->result_array();
		}else{
			return false;
		}		
	}

	public function getCorrectAnswerByQuestion($question){
		$this->db->select('*');
		$this->db->where('question_id',$question);
		$this->db->where('correct_answer',1);	
		$sql = $this->db->get('answers');
		if($sql){
			$data = $sql->row_array();
			if(!empty($data)){
				return $data['id'];
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * USE : Store to Quiz data
	 */
	public function saveQuiz($data){
		$sql = $this->db->insert('user_answer',$data);
		if($sql){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	/**
	 * USE : Count user correct Answer
	 */
	public function countCorrectAnswer(){
		$this->db->select('count(id) as correct');
		$this->db->where('correct_answer',1);	
		$sql = $this->db->get('user_answer');
		$data = $sql->row_array();
		return $data['correct'];
	}

	/**
	 * USE : Delete Quiz
	 */
	public function deleteQuiz(){
		$this->db->where('user_id',$this->session->userdata('userid'));
		$sql = $this->db->delete('user_answer');

		return $sql;
	}
}
