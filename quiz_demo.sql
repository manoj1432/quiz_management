-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 12, 2020 at 06:26 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` text,
  `correct_answer` tinyint(4) NOT NULL COMMENT '0 : wrong   1:true',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `answer`, `correct_answer`) VALUES
(1, 1, 'Server-side', 1),
(2, 1, 'Client-side', 0),
(3, 1, 'Browser-side', 0),
(4, 1, 'In-side', 0),
(5, 2, '$ask', 1),
(6, 2, '$post', 0),
(7, 2, '$request', 0),
(8, 2, '$get', 0),
(9, 3, 'ord();', 1),
(10, 3, 'chr();', 0),
(11, 3, 'asc();', 0),
(12, 3, 'val();', 0),
(13, 4, 'Get', 1),
(14, 4, 'Post', 0),
(15, 4, 'Both', 0),
(16, 4, 'None', 0),
(17, 5, 'toupper($var)', 0),
(18, 5, 'upper($var)', 0),
(19, 5, 'ucword($var)', 0),
(20, 5, 'ucwords($var)', 1),
(21, 6, 'strlen($variable)', 1),
(22, 6, 'count($variable)', 0),
(23, 6, 'strcount($variable)', 0),
(24, 6, 'len($variable)', 0),
(25, 7, 'search()', 0),
(26, 7, 'locate()', 0),
(27, 7, 'strpos()', 1),
(28, 7, 'None of the above.', 0),
(29, 8, 'rsort()', 1),
(30, 8, 'sort()', 0),
(31, 8, 'shuffle()', 0),
(32, 8, 'reset()', 0),
(33, 9, 'fopen()', 0),
(34, 9, 'fread()', 1),
(35, 9, 'filesize()', 0),
(36, 9, 'file_exist()', 0),
(37, 10, '$GLOBALS', 0),
(38, 10, '$_SERVER', 0),
(39, 10, '$_COOKIE', 1),
(40, 10, '$_SESSION', 0);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `question`) VALUES
(1, 'PHP is an example of ___________ scripting language.'),
(2, 'Which of the following variables is not a predefined variable?'),
(3, 'When you need to obtain the ASCII value of a character which of the following function you apply in PHP?'),
(4, 'Which of the following method sends input to a script via a URL?'),
(5, 'Which of the following function returns a text in title case from a variable?'),
(6, 'Which of the following function returns the number of characters in a string variable?'),
(7, 'Which of the following function is used to locate a string within a string?'),
(8, 'Which of the following function sorts an array in reverse order?'),
(9, 'Which of the following function is used to read the content of a file?'),
(10, 'Which of the following is an associative array of variables passed to the current script via HTTP cookies?');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(1, 'manoj@silverwebbuzz.com', 'e10adc3949ba59abbe56e057f20f883e'),
(2, 'manoj@silverwebbuzz.com', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `user_answer`
--

DROP TABLE IF EXISTS `user_answer`;
CREATE TABLE IF NOT EXISTS `user_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `quiestion_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `correct_answer` tinyint(1) NOT NULL COMMENT '0 - wrong 1 = True',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
